import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  showContent: boolean = false;
  posts = [
    {
      content: 'This is the first post content.',
      likes: 50,
      shares: 10,
      comments: 5,
    },
    {
      content: 'This is the first post content.',
      likes: 50,
      shares: 10,
      comments: 5,
    },
    {
      content: 'This is the first post content.',
      likes: 50,
      shares: 10,
      comments: 5,
    },
    {
      content: 'This is the first post content.',
      likes: 50,
      shares: 10,
      comments: 5,
    },
    // Add more post objects here
  ];

  showHomeContent(){
    this.showContent = true;
  }
  
}

