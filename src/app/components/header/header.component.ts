import { Component , ElementRef, HostListener } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  isMenuVisible = false;
  constructor(private element: ElementRef){}

  // showFiltersMenu(){
  //   this.isMenuVisible = !this.isMenuVisible;
  // }

  // @HostListener('document:click', ['$event'])
  // onDocumentClick(event: MouseEvent){
  //   const target = event.target as HTMLElement;
  //   const searchBar = this.element.nativeElement.querySelector('.search-bar');
  //   const menu = this.element.nativeElement.querySelector('.filters-menu');
    
  //   if(!searchBar.contains(target) && !menu.contains(target)){
  //     this.isMenuVisible = false;
  //   }
  // }

}
