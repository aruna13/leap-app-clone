import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatIconModule} from '@angular/material/icon';
import { AnimateFilterWindowComponent } from './components/animate-filter-window/animate-filter-window.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AddPlusButtonComponent } from './components/add-plus-button/add-plus-button.component';
import { PostComponent } from './components/post/post.component';
import { HomeComponent } from './components/home/home.component';
import { ProsComponent } from './components/pros/pros.component';
import { CirclesComponent } from './components/circles/circles.component';
import { EventsComponent } from './components/events/events.component';

@NgModule({
  declarations: [
    AppComponent,
    AnimateFilterWindowComponent,
    HeaderComponent,
    FooterComponent,
    AddPlusButtonComponent,
    PostComponent,
    HomeComponent,
    ProsComponent,
    CirclesComponent,
    EventsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  
 }
