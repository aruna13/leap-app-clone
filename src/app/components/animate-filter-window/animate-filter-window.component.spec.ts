import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimateFilterWindowComponent } from './animate-filter-window.component';

describe('AnimateFilterWindowComponent', () => {
  let component: AnimateFilterWindowComponent;
  let fixture: ComponentFixture<AnimateFilterWindowComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AnimateFilterWindowComponent]
    });
    fixture = TestBed.createComponent(AnimateFilterWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
